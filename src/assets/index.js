import womanImage from './images/portrait-smiling-asian-businesswoman.png'
import logo from './images/logo.png'
import award from './images/award.png'
import profileImg from './images/profile.png'
import process1 from './images/process-1.svg'
import process2 from './images/process-2.svg'
import process3 from './images/process-3.svg'
import process4 from './images/process-4.png'
import slide2 from './images/slide2.png'

export {womanImage,logo,award,profileImg,process1,process2, process3, process4,slide2}