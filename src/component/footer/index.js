import React from 'react'
import { Container,Col,Row } from 'reactstrap'
import './footer.scss'
import { FaFacebookF,FaInstagram } from 'react-icons/fa'


const Footer = () => {
    return (
        <div id="footerSection" className="p-5">
            <Container>
                <Row>
                    <Col>
                        Koperasi Simpan Pinjam Teman Jakarta
                    </Col>
                </Row>
                <Row className="mt-5">
                    <Col>
                        <Row>
                            <Col>
                                <h6>Kantor Pusat</h6>
                                <p>Gedung Satia Budi</p>
                            </Col> 
                            <Col>
                                <h6>Kantor Cabang</h6>
                                <p>Gedung Satia Budi</p>
                            </Col>
                        </Row>
                    </Col>
                    <Col>
                        <Row>
                            <Col>
                                <h6>Tentang Koperasi</h6>
                                <ul>
                                    <li>Tentang Rangkul Teman</li>
                                    <li>Sambutan Direktur</li>
                                </ul>
                            </Col>
                            <Col>
                                <h6>Layanan</h6>
                                <ul>
                                    <li>Simpanan</li>
                                    <li>Pinjaman</li>
                                </ul>
                            </Col>
                            <Col>
                                <h6>Ikuti Kami</h6>
                                <FaFacebookF className="sosmed"/>
                                <FaInstagram className="sosmed"/>
                                
                                <h6>Call Center</h6>
                                <span class="opacity-5 phone-number">
                                    150210
                                </span>
                            </Col>
                        </Row>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default Footer
