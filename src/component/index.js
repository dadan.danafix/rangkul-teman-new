import Header from './header'
import Jumbo from './jumbo'
import Profile from './profile'
import Procedure from './procedure'
import News from './news'
import Footer from './footer'
export {Header,Jumbo, Profile,Procedure,News,Footer}