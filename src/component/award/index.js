import React from 'react'
import { Container,Col,Row } from 'reactstrap'
import { award } from '../../assets'
import './award.scss'

const Award = () => {
    return (
        <Container id="award-section">
            <Row className="row">
                <Col id="award-left" className="p-5">
                    <img src={award} />
                </Col>
                <Col id="award-right" className="p-5 align-self-center">
                    <p id="subtitle">DARI KAMI</p>
                    <h4>Menerima Award dari Indonesia Award Magazine</h4>
                    <p>Sebagai hasil dari kerja keras semua jajaran KSP Rangkul Teman, untuk tahun 2021 KSP Rangkul Teman menerima Award dalam kategori “Highly Recommended Cooperative 2021” dari Indonesia Award Magazine</p>
                </Col>
            </Row>
        </Container>
    )
}

export default Award
