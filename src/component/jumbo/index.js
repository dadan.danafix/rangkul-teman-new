import React from 'react'
import { Row,Col, Button, Container,Card, CardBody,
    CardTitle, CardSubtitle } from 'reactstrap'
import { womanImage,slide2 } from '../../assets'
import Slider from "react-slick";
import './jumbo.scss'


const Jumbo = () => {

    const slide = [
        {
          id    : "1",
          title : "Simpanan Berjangka",
          desc  : "10% per tahun. Get interest monthly",
          image : womanImage,
          dot   : "Simpanan berjangka",
          dotsub : 'Hingga 50 juta. 6-12 bulan',
          backColor: "#EBEBF4"
        },
        {
          id    : "2",
          title : "Pinjaman koperasi mudah dan aman",
          desc  :"Pinjaman hingga 50jt, 6-12 bulan",
          image : slide2,
          dot  : "Pinjaman Koperasi",
          dotsub  : "10% per tahun",
          backColor: "#F0C59B"
        },
      ];
    

    const settings = {
        dots: true,
        infinite: true,
        speed: 500,
        slidesToShow: 1,
        slidesToScroll: 1,
        appendDots: dots => (
            <Row>
              <Col style={{ margin: "50px" }}> {dots} </Col>
            </Row>
          ),
          customPaging: i => (
            <Card style={{'minWidth':'400px'}}>
                <CardBody>
                    <CardTitle tag="h5">{slide[i].dot}</CardTitle>
                    <CardSubtitle tag="h6" className="mb-2 ">{slide[i].dotsub}</CardSubtitle>
                </CardBody>
            </Card>
          )
      };
    return (
        <Slider {...settings} className="mb-5">
            {
                slide.map((s,key)=>{
                    return (<div key={key} >
                        <div style={{'background':s.backColor,'height':'60vh'}}>
                            <Container className="pt-5">
                                <Row className="">
                                    <Col className="containerText">
                                        <h1 className="slideTitle">{s.title}</h1>
                                        <p className="slideDesc">{s.desc}</p>
                                        <Button className="btnSlide" color="warning">Daftar</Button>{' '}
                                    </Col>
                                    <Col className="container-img">
                                        <img src={s.image}/>
                                    </Col>
                                </Row>
                            </Container>
                        </div>
                    </div>)
                })
            }
        </Slider>
    )
}

export default Jumbo
