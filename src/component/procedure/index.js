import React from 'react'
import { Container, Row,Col, Card, CardText, CardBody, CardTitle, CardSubtitle, Button } from 'reactstrap'
import { process1,process2,process3,process4 } from '../../assets'
import './procedure.scss'

const Procedure = () => {
    return (
       <div  id="procedureSection">
            <Container className="p-5">
                <Row>
                    <Col>
                            <h3 className="title-how-to p-3">Bagaimana menjadi anggota Koperasi Rangkul Teman?</h3>
                    </Col>
                </Row>
                <Row className="mt-2 mb-4">
                    <Col>
                            <Card className="p-4">
                                <CardBody>
                                    <Row>
                                        <Col sm='2'>
                                            <span className="number-my-card">01</span>
                                        </Col>
                                        <Col>
                                            <CardSubtitle tag="h6" className="mb-2 brown-text">Card subtitle</CardSubtitle>
                                            <CardTitle className="title-my-card" tag="h5">Card title</CardTitle>
                                            <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                                        </Col>
                                    </Row>
                                </CardBody>
                            </Card>
                    </Col>
                    <Col>
                            <img src={process1}/>
                    </Col>
                </Row>
                <Row className="rowCardLeft">
                        <div className="col-lg-12">
                            <div className="row justify-content-center">
                                <div className="col-6 col-lg-3 m-0 p-0">
                                    <div className="line-bottom position-relative">
                                        <img className="arrow-line" src="https://rangkulteman.id/wp-content/themes/rangkulteman/assets/img/arrow-line.png" loading="lazy"/>
                                    </div>
                                </div>
                                <div className="col-6 col-lg-3 m-0 p-0">
                                    <div className="line-bottom position-relative">
                                        <img className="arrow-down" src="https://rangkulteman.id/wp-content/themes/rangkulteman/assets/img/arrow-down.png" loading="lazy"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                </Row>
                <Row className="rowCardRight">
                    <Col>
                            <img className="img-fluid" src={process2}/>
                    </Col>
                    <Col>
                        <Card className="p-4">
                            <CardBody>
                                <Row>
                                    <Col sm='2'>
                                        <span className="number-my-card">02</span>
                                    </Col>
                                    <Col>
                                        <CardSubtitle tag="h6" className="mb-2 brown-text">Card subtitle</CardSubtitle>
                                        <CardTitle className="title-my-card" tag="h5">Card title</CardTitle>
                                        <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
                <Row>
                <div className="col-lg-12"><div className="row justify-content-center"><div className="col-6 col-lg-3 m-0 p-0"><div className="line-bottom position-relative"><img className="arrow-down-r" src="https://rangkulteman.id/wp-content/themes/rangkulteman/assets/img/arrow-down.png" loading="lazy"/></div></div><div className="col-6 col-lg-3 m-0 p-0"><div className="line-bottom position-relative"><img className="arrow-line-r" src="https://rangkulteman.id/wp-content/themes/rangkulteman/assets/img/arrow-line.png" loading="lazy"/></div></div></div></div>
                </Row>
                <Row className="rowCardLeft">
                    <Col>
                        <Card className="p-4">
                            <CardBody>
                                <Row>
                                    <Col sm='2'>
                                        <span className="number-my-card">03</span>
                                    </Col>
                                    <Col>
                                        <CardSubtitle tag="h6" className="mb-2 brown-text">Card subtitle</CardSubtitle>
                                        <CardTitle className="title-my-card" tag="h5">Card title</CardTitle>
                                        <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                                    </Col>
                                </Row>
                            </CardBody>
                        </Card>
                    </Col>
                    <Col>
                            <img className="img-fluid" src={process3}/>
                    </Col>
                </Row>
                <Row className="rowCardRight">
                        <div className="col-lg-12">
                            <div className="row justify-content-center">
                                <div className="col-6 col-lg-3 m-0 p-0">
                                    <div className="line-bottom position-relative">
                                        <img className="arrow-line" src="https://rangkulteman.id/wp-content/themes/rangkulteman/assets/img/arrow-line.png" loading="lazy"/>
                                    </div>
                                </div>
                                <div className="col-6 col-lg-3 m-0 p-0">
                                    <div className="line-bottom position-relative">
                                        <img className="arrow-down" src="https://rangkulteman.id/wp-content/themes/rangkulteman/assets/img/arrow-down.png" loading="lazy"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                </Row>
                <Row className="my-2">
                    <Col>
                            <img className="img-fluid" src={process4}/>
                    </Col>
                    <Col>
                            <Card className="p-4">
                                <CardBody>
                                    <Row>
                                        <Col sm='2'>
                                            <span className="number-my-card">04</span>
                                        </Col>
                                        <Col>
                                            <CardSubtitle tag="h6" className="mb-2 brown-text">Card subtitle</CardSubtitle>
                                            <CardTitle className="title-my-card" tag="h5">Card title</CardTitle>
                                            <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                                        </Col>
                                    </Row>
                                </CardBody>
                            </Card>
                    </Col>
                </Row>
                <Row>
                    <Col className="text-center mt-5">
                            <Button>Daftar Sekarang</Button>
                    </Col>
                </Row>
            </Container> 
       </div>
    )
}

export default Procedure
