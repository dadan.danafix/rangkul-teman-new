import React from 'react'
import { Col, Container, Row } from 'reactstrap'
import { profileImg } from '../../assets'
import './profile.scss'

const Profile = () => {
    return (
        <Container id="profileSection" className="p-5">
            <Row>
                <Col className="p-5">
                    <p id="subtitle">Koperasi Rangkul Teman</p>
                    <h2>Komunitas yang beragam dan memberdayakan</h2>
                    <p>
                        Koperasi Rangkul Teman merupakan komunitas yang percaya bahwa semangat kebersamaan dan gotong royong adalah pusat dari Gerakan Koperasi dan kunci dari pertumbuhan ekonomi, serta mengusung keragaman dan pemberdayaan bagi tiap anggotanya.
                    </p>
                </Col>
                <Col>
                    <img src={profileImg}/>
                </Col>
            </Row>
            
        </Container>
    )
}

export default Profile
