import React from 'react'
import { Container, Col, Row,Card, CardText, CardBody, CardLink, CardTitle, CardSubtitle, Button } from 'reactstrap'
import './news.scss'
const News = () => {
    return (
       <Container className="py-5" id="sectionNews">
           <Row>
                <Col>
                    <h3 className="title-blue">Berita Terkini</h3>
                </Col>
           </Row>
           <Row>
                <Col sm='4'>
                    <Card>
                        <img width="100%" src="https://via.placeholder.com/300x250" alt="Card image cap" />
                        <Row className="justify-content-end mt-2">
                            <Col sm='5'>
                                <Button className="btnCategory" outline color="warning">Kesehatan</Button>
                            </Col>
                        </Row>
                        <CardBody>
                            <CardTitle className="card-title-blue" tag="h5">Card title</CardTitle>
                            <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                        </CardBody>
                    </Card>
                </Col>
                <Col sm='4'>
                    <Card>
                        <img width="100%" src="https://via.placeholder.com/300x250" alt="Card image cap" />
                        <Row className="justify-content-end mt-2">
                            <Col sm='5'>
                                <Button className="btnCategory" outline color="warning">Kesehatan</Button>
                            </Col>
                        </Row>
                        <CardBody>
                            <CardTitle className="card-title-blue" tag="h5">Card title</CardTitle>
                            <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                        </CardBody>
                    </Card>
                </Col>
                <Col sm='4'>
                    <Card>
                        <img width="100%" src="https://via.placeholder.com/300x250" alt="Card image cap" />
                        <Row className="justify-content-end mt-2">
                            <Col sm='5'>
                                <Button className="btnCategory" outline color="warning">Kesehatan</Button>
                            </Col>
                        </Row>
                        <CardBody>
                            <CardTitle className="card-title-blue" tag="h5">Card title</CardTitle>
                            <CardText>Some quick example text to build on the card title and make up the bulk of the card's content.</CardText>
                        </CardBody>
                    </Card>
                </Col>
           </Row>
           <Row>
               <Col className="text-center m-5">
                <Button>Berita Lain</Button>
               </Col>
           </Row>
       </Container>
    )
}

export default News
