import React, { useState } from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  NavbarText, Container
} from 'reactstrap';
import { logo } from '../../assets';
import './header.scss'

const Header = (props) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  return (
    <Container id="headerSection">
      <Navbar light expand="md">
        <NavbarBrand href="/">
            <img src={logo}/>
        </NavbarBrand>
        <NavbarToggler onClick={toggle} />
        <Collapse isOpen={isOpen} navbar>
          <Nav className="me-auto" navbar>
            <NavItem>
              <NavLink href="#">Tentang Koperasi</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#">Layanan</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#">Kenggotaan</NavLink>
            </NavItem>
            <NavItem>
              <NavLink href="#">Hubungi Kami</NavLink>
            </NavItem>
          </Nav>
          <NavbarText>Hi, Eghi</NavbarText>
        </Collapse>
      </Navbar>
    </Container>
  );
}

export default Header;