import logo from './logo.svg';
import './App.css';
import { Header, Jumbo, Profile,Procedure, News, Footer } from './component';
import Award from './component/award';

function App() {
  return (
    <>
      <Header />
      <Jumbo/>
      <Award/>
      <Profile/>
      <Procedure/>
      <News />
      <Footer />
    </>
  );
}

export default App;
